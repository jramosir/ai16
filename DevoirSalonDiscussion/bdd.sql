DROP TABLE IF EXISTS `ai16_users`;
CREATE TABLE `ai16_users` (
  `id_user` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `admin` boolean NOT NULL DEFAULT false,
  `active` boolean NOT NULL DEFAULT true,
  PRIMARY KEY (`id_user`)
);

DROP TABLE IF EXISTS `ai16_channels`;
CREATE TABLE `ai16_channels` (
  `id_channel` int NOT NULL AUTO_INCREMENT,
  `id_owner` int NOT NULL,
  `channel_name` varchar(200) NOT NULL,
  `description` text,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `start_hour` time NOT NULL,
  `end_hour` time NOT NULL,
  PRIMARY KEY (`id_channel`),
  KEY `id_owner` (`id_owner`),
  FOREIGN KEY (`id_owner`) REFERENCES `ai16_users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `ai16_channel_user_associations`;
CREATE TABLE `ai16_channel_user_associations` (
  `id_user` int NOT NULL,
  `id_channel` int NOT NULL,
  FOREIGN KEY (`id_user`) REFERENCES `ai16_users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`id_channel`) REFERENCES `ai16_channels` (`id_channel`) ON DELETE CASCADE ON UPDATE CASCADE
);

DROP TABLE IF EXISTS `ai16_messages`;
CREATE TABLE `ai16_messages` (
    `id_message` int PRIMARY KEY AUTO_INCREMENT,
    `sender` varchar(100) NOT NULL,
    `id_channel` int NOT NULL,
    `message` varchar(200) NOT NULL,
    `date` date NOT NULL,
    `hour` time NOT NULL,
	FOREIGN KEY (`id_channel`) REFERENCES `ai16_channels` (`id_channel`) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose1','ramos1','jose1.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose2','ramos2','jose2.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose3','ramos3','jose3.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose4','ramos4','jose4.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose5','ramos5','jose5.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose6','ramos6','jose6.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose7','ramos7','jose7.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose8','ramos8','jose8.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('jose9','ramos9','jose9.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', false);
INSERT INTO ai16_users(first_name,last_name,mail,password,gender,admin) VALUES ('santiago','ramos','santiago.ramos@utc.fr','$2a$10$SWr3m4RJSXoRe83FEKQQauxC2dN1q9RiXooCrjShU1sslLvTayFmG','Masculin', true);


INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (1,'jramos1','salontest1','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (2,'jramos2','salontest2','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (3,'jramos3','salontest3','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (4,'jramos4','salontest4','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (5,'jramos5','salontest5','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (6,'jramos6','salontest6','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (7,'jramos7','salontest7','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (8,'jramos8','salontest8','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (9,'jramos9','salontest9','2022-05-28', '2022-07-28', '00:01', '23:59');
INSERT INTO ai16_channels(id_owner,channel_name,description,start_date,end_date,start_hour, end_hour) VALUES (10,'AI16 PROJET','Discussion et planification ai16','2022-05-28', '2022-07-28', '00:01', '23:59');

INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (1,2);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (2,3);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (3,4);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (4,5);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (5,6);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (6,7);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (7,8);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (8,9);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (9,1);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,1);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,2);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,3);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,4);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,5);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,6);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,7);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,8);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (10,9);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (1,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (2,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (3,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (4,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (5,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (6,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (7,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (8,10);
INSERT INTO ai16_channel_user_associations(id_user,id_channel) VALUES (9,10);

INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose1',10,'first','2022-07-22', '00:01');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose2',10,'second','2022-07-22', '00:02');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose3',10,'third','2022-07-22', '00:03');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose4',10,'four','2022-07-22', '00:04');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose5',10,'five','2022-07-22', '00:05');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose6',10,'six','2022-07-22', '00:06');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose7',10,'seven','2022-07-22', '00:07');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose8',10,'eight','2022-07-22', '00:08');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('jose9',10,'nine','2022-07-22', '00:09');
INSERT INTO ai16_messages(sender,id_channel, message, date, hour) VALUES ('santiago',10,'ten','2022-07-22', '00:10');
