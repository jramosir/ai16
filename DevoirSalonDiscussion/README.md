# Projet AI16 - Salon de Discussion

## Description
Salon de Discussion web en utilisant les technologies Spring Boot et Spring MVC avec hibernate pour la persistance de données et thymeleaf pour la vue.

## Installation
Importer le dossier ai16/SalonDiscussion sous eclipse comment projet maven.
Pour la Base de données utiliser MySQL sur le port 3306 et créer une nouvelle Databsae ai16_salondiscussion.
Creer un utilisateur avec droits d'administration sur votre BDD nommé "springuser" avec un password "springpassword"
Autrement il faudra modifier le fichier application.properties avec votre utlisateur root ou autre.
Executer le fichier BDD.sql pour l'initialization des tables.

On suppose que le serveur locale Tomcat v9 est installé.
Pour monter le server il suffit d'executer le fichier SalonDiscussionApplication.java qui lance spring Boot sur le serveur.

S'assurer que les configurations sont correctes pour votre environement dans les fichiers pom.xml et application.properties

##### Exploitation
Les mot de passe pour les comptes de test dans la bdd est "123456789"

##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```
##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
