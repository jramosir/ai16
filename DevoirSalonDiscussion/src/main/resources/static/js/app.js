let DATATABLE_OPTIONS = {
	"language": {
		"info": "Afficher les éléments de _START_ à _END_ (_TOTAL_ au total)",
		"search": "Rechercher",
		"lengthMenu": "Afficher _MENU_ lignes",
		"infoEmpty": "Aucun résultat",
		"infoFiltered": "(filtré sur _MAX_ entrées totales)",
		"zeroRecords": "Aucun résultat",
		paginate: {
			first: "Premier",
			previous: "Pr&eacute;c&eacute;dent",
			next: "Suivant",
			last: "Dernier"
		},
	},
	"lengthMenu": [ 5, 10, 20 ]
};

