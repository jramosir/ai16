// VARIABLES GLOBALES
let MODAL_EDITUSER = "#modal-editUser";
let MODAL_EDITUSER_BTN_SAVE = "#btn-editUser-save";
let MODAL_EDITUSER_BTN_CANCEL = "#btn-editUser-cancel";
let MODAL_DELETEUSER = "#modal-deleteUser";
let MODAL_DELETEUSER_BTN_CONFIRM = "#btn-deleteUser-confirm";
let MODAL_DELETEUSER_BTN_CANCEL = "#btn-deleteUser-cancel";
let FORM_USER = "#form-user";

function onClickEditUser(userID) {
	// récupération des infos de l'utilisateur sur la ligne
	let tableRow = $('#tableUsersList').find("tr[user-id='"+userID+"']")[0];
	let firstName = $(tableRow.children[1]).html();
	let lastName = $(tableRow.children[2]).html();
	let email = $(tableRow.children[3]).html();
	let gender = $(tableRow.children[4]).html();
	let isAdmin = $(tableRow.children[5]).html() == 'Admin';
	let isActive = $(tableRow.children[6]).html() == 'Activé';
	
	// modification de la modale
	$(MODAL_EDITUSER + " input#userID").val(userID)
	$(MODAL_EDITUSER + " input#firstName").val(firstName)
	$(MODAL_EDITUSER + " input#firstName").val(firstName)
	$(MODAL_EDITUSER + " input#lastName").val(lastName)
	$(MODAL_EDITUSER + " input#mail").val(email)
	$(MODAL_EDITUSER + " select#gender option[value='" + gender + "']").attr('selected', 'selected');
	$(MODAL_EDITUSER + " input#admin").attr('checked', isAdmin);
	$(MODAL_EDITUSER + " input#active").attr('checked', isActive);

	// affichage de la modale
	$(MODAL_EDITUSER_BTN_SAVE).attr("disabled", false);
	$(MODAL_EDITUSER_BTN_CANCEL).attr("disabled", false);
	$(MODAL_EDITUSER).modal('toggle');
}

function onClickDeleteUser(userID) {
	// modification de la modale
	$(MODAL_DELETEUSER + " input#userID").val(userID);
	
	// affichage de la modale
	$(MODAL_DELETEUSER_BTN_CONFIRM).attr("disabled", false);
	$(MODAL_DELETEUSER_BTN_CANCEL).attr("disabled", false);
	$(MODAL_DELETEUSER).modal('toggle');
}

function onClickSaveModifications() {
	$(MODAL_EDITUSER_BTN_SAVE).attr("disabled", true);
	$(MODAL_EDITUSER_BTN_CANCEL).attr("disabled", true);
	
	// récupération des infos du formulaire
	let userID = $(MODAL_EDITUSER + " input#userID").val();
	let firstName = $(MODAL_EDITUSER + " input#firstName").val();
	let lastName = $(MODAL_EDITUSER + " input#lastName").val();
	let mail = $(MODAL_EDITUSER + " input#mail").val();
	let gender = $(MODAL_EDITUSER + " select#gender option:selected").val();
	let isAdmin = $(MODAL_EDITUSER + " input#admin").is(":checked");
	let isActive = $(MODAL_EDITUSER + " input#active").is(":checked");

	$.post("/admin/edit-user",
		{
			idUser : userID,
			firstName : firstName,
			lastName : lastName,
			mail : mail,
			gender : gender,
			admin : isAdmin,
			active: isActive
		}
	).done(function() {
		// si la modification s'est bien effectuée, on ferme la popup
		$(MODAL_EDITUSER).modal('toggle');
		
		// et on met à jour le tableau
		let tableRow = $('#tableUsersList').find("tr[user-id='"+userID+"']");
		$(tableRow).find("td.firstName").html(firstName);
		$(tableRow).find("td.lastName").html(lastName);
		$(tableRow).find("td.mail").html(mail);
		$(tableRow).find("td.gender").html(gender);
		if(isAdmin){
			$(tableRow).find("td.role").html("Admin");
		} else {
			$(tableRow).find("td.role").html("Membre");
		}
		if(isActive){
			$(tableRow).find("td.active").html("Activé");
		} else {
			$(tableRow).find("td.active").html("Désactivé");
		}
	});
}

function onClickConfirmDeletion() {
	// récupération des infos dans la modale
	let userID = $(MODAL_DELETEUSER + " input#userID").val();

	$.ajax({
		url : "/admin/delete-user/"+userID,
		type : "DELETE",
		success : function() {
			window.location = "/admin/users-list";
		}
	});
}

function onClickCreateUser() {
	// récupération des infos du formulaire
	let firstName = $(FORM_USER + " input#firstName").val();
	let lastName = $(FORM_USER + " input#lastName").val();
	let mail = $(FORM_USER + " input#mail").val();
	let gender = $(FORM_USER + " select#gender option:selected").val();
	let isAdmin = $(FORM_USER + " input#admin").is(":checked");
	let isActive = $(FORM_USER + " input#active").is(":checked");
	
	// todo: rajouter les controles sur les champs : champ requis, adresse email, champs corrects...
	
	$.post("/admin/create-user",
		{
			firstName : firstName,
			lastName : lastName,
			mail : mail,
			gender : gender,
			admin : isAdmin,
			active: isActive
		}
	).done(function() {
		window.location = "/admin/users-list";
	});
}

$(document).ready(function() {
	// Initialisation du datatable
	let table = new DataTable("#tableUsersList", DATATABLE_OPTIONS);

	$('button.save-modifications').on('click', function() {
		onClickSaveModifications();
	});
	
	$('button.confirm-deletion').on('click', function() {
		onClickConfirmDeletion();
	});
})


