var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var userArea = document.querySelector('#userArea');
var connectingElement = document.querySelector('#connecting');
var container = document.querySelector('#container');
let height = window.innerHeight;

var stompClient = null;
var username = null;
var channel = null; 
 
container.style.height = height;
function connect() {
    username = document.querySelector('#username').innerText.trim();
    channel = document.querySelector('#channel').innerText.trim();
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);

    stompClient.connect({}, onConnected, onError);
}

// Connect to WebSocket Server.
connect();

function onConnected() {
	let today = new Date().toISOString().slice(0, 10);
	let now = new Date().toISOString().slice(11, 19);
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/publicChatRoom', onMessageReceived);

    // Tell your username to the server
    var serverMessage = {
            sender: "SpringClient",
			idChannel: channel,
            message: username + "- has connected",
			date: today,
			hour: now
        };
    stompClient.send("/app/chat.addUser",{},JSON.stringify(serverMessage));
    connectingElement.classList.add('hidden');
}


function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}


function sendMessage(event) {
    var messageContent = messageInput.value.trim();
    let today = new Date().toISOString().slice(0, 10);
	let now = new Date().toISOString().slice(11, 19);
    
    if(messageContent && stompClient) {
        var chatMessage = {
            sender: username,
			idChannel: channel,
            message: messageInput.value,
			date: today,
			hour: now
        };
        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}


function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);
	let today = new Date().toISOString().slice(0, 10);
	let now = new Date().toISOString().slice(11, 19);
	//paso del mago que servira mas tarde
	if(message.idChannel == channel){
		if(message.sender == "SpringClient"){
			var senderId = message.message.split('-')[0];
			var flag = document.getElementById(senderId);
			if(flag==null){
				if (senderId != username){
				//add server message to view
				var messageElement = document.createElement('div');
				messageElement.classList.add('group-rom');
			    var contentElement = document.createElement('div');
				contentElement.classList.add('server-part');
				var contentText = document.createTextNode(message.message);
			    contentElement.appendChild(contentText);
			    messageElement.appendChild(contentElement);
			    
			   	messageArea.append(messageElement);
			    messageArea.scrollTop = messageArea.scrollHeight;
			    //add new connected user to list
			    var divElement = document.createElement('div');
			    divElement.classList.add('group-rom');
			    var userElement = document.createElement('i');
			    userElement.setAttribute('id', senderId);
			    userElement.classList.add("fa","fa-circle","text-success");
			    divElement.appendChild(userElement);
			    var usernameText = document.createTextNode(senderId);
			    divElement.appendChild(usernameText);
			    userArea.append(divElement);
			    userArea.scrollTop = messageArea.scrollHeight;
			    console.log("got newUser:", senderId);
				}
			}
		} else if(message.sender == "SpringServer"){
			//add server message to view
			var messageElement = document.createElement('div');
			messageElement.classList.add('group-rom');
		    var contentElement = document.createElement('div');
			contentElement.classList.add('server-part');
			var contentText = document.createTextNode(message.message);
		    contentElement.appendChild(contentText);
		    messageElement.appendChild(contentElement);
			messageArea.append(messageElement);
			messageArea.scrollTop = messageArea.scrollHeight;
			//delete disconnected user from list
			var senderId = message.message.split('-')[0];
			var element = document.getElementById(senderId);
			element.parentNode.remove();
			console.log("remove user:", senderId);
		} else {
			//formatting and sending a new message on chat
			var messageElement = document.createElement('div');
			messageElement.classList.add('group-rom');
			var senderElement = document.createElement('div');
			senderElement.classList.add('first-part');
			var usernameText = document.createTextNode(message.sender);
		    senderElement.appendChild(usernameText);
		    messageElement.appendChild(senderElement);
		    
		    var contentElement = document.createElement('div');
			contentElement.classList.add('second-part');
			var contentText = document.createTextNode(message.message);
		    contentElement.appendChild(contentText);
		    messageElement.appendChild(contentElement);
		    
		    var dateElement = document.createElement('div');
			dateElement.classList.add('third-part');
			var dateText = document.createTextNode(message.date+'\n');
			var hourText = document.createTextNode(message.hour);
		    dateElement.appendChild(dateText);
		    dateElement.appendChild(hourText);
		    messageElement.appendChild(dateElement);
		
		    messageArea.append(messageElement);
		    messageArea.scrollTop = messageArea.scrollHeight;
		    console.log("got message:", message.message);
		}		
	}
}
 
 
messageForm.addEventListener('submit', sendMessage, true);
