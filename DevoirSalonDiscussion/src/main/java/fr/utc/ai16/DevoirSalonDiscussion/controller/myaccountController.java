package fr.utc.ai16.DevoirSalonDiscussion.controller;

import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;

@Controller
public class myaccountController {
	@Autowired
	userRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping("/my_account")
	public String showMyAccount(Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
        model.addAttribute("user", loggedUser);
		return "my_account";
	}
	
	@GetMapping("/modify")
	public String showModifyUserForm(Model model, Principal principal) {
		//view with form
		return "modify";
	}
	
	@PostMapping("/modifyP-save")
	public String showPasswordForm(@RequestParam("newPassword") String newpassword, Model model, Principal principal, HttpServletRequest request) throws ServletException {
		String encodedPassword = passwordEncoder.encode(newpassword);
		user loggedUser = userRepository.getUserByMail(principal.getName());
		loggedUser.setPassword(encodedPassword);
		userRepository.save(loggedUser);
		request.logout();
		return "login";
	}

	@PostMapping("/modify-save")
	public String modifyUser( @ModelAttribute("user") user user, Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
		loggedUser.setFirstName(user.getFirstName());
		loggedUser.setLastName(user.getLastName());
		loggedUser.setGender(user.getGender());
		userRepository.save(loggedUser);
		//view with form
		return "modify";
	}
	
	@PostMapping("/deactivate-save")
	public String deactivateAccount(Model model, Principal principal, HttpServletRequest request) throws ServletException {
		user loggedUser = userRepository.getUserByMail(principal.getName());
		//somehow sql default true value not applied when creating new user... add manually
		loggedUser.setActive(false);
		userRepository.save(loggedUser);
		request.logout();
		return "login";
	}
	
	private boolean userExists( String mail )
    {
        
    	System.out.println( "Checking if user exists: " + mail );
        
        // check the database if the user already exists
    	boolean exists;
        if (userRepository.getUserByMail(mail) == null) {
        	exists = false;
        }
        else {
        	exists = true;
        }
        System.out.println( "User: " + mail + ", exists: " + exists );
        return exists;
    }
	
}
