package fr.utc.ai16.DevoirSalonDiscussion.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.utc.ai16.DevoirSalonDiscussion.models.channel;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

public interface channelRepository extends JpaRepository<channel, Long> {
	
	List <channel> findByChannelName(String channelName);
	
	List <channel> findByIdOwner(Long idOwner);
		
	@Query("SELECT u FROM channel u WHERE u.idOwner = :idOwner AND u.channelName = :channelName")
	public channel findByIdOwnerAndChannelName(@Param("idOwner") Long idOwner, @Param("channelName") String channelName);
}
