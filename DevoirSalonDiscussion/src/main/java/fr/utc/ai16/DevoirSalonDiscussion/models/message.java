package fr.utc.ai16.DevoirSalonDiscussion.models;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Santiago RAMOS
 * This class is a spring entity used to map a message class object with a user database table in order to implement CRUD methods efficiently
 * ATTENTION: Naming convention in important while using this god forsaken framework otherwise repository methods don't work........
 */
@Entity
@Table(name = "ai16_messages")
public class message {
	
	/*to generate increasing values for new users id
	 *link to database column is automatically resolved if the attribute name is the same
	 *otherwise it must be declared
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_message")
	private Long idMessage;
	private String message;
	private Date date;
	private Time hour;
	@Column(name="id_channel")
	private Long idChannel;
	@Column(name="sender")
	private String sender;
	
	public message(){
		//Default constructor with no argument.
		//no overloaded constructor because all values are not null in the table, therefore all values must be filled anyways.
	}

	public Long getIdMessage() {
		return idMessage;
	}

	/**
	 * @return the idChannel
	 */
	public Long getIdChannel() {
		return idChannel;
	}

	/**
	 * @param idChannel the idChannel to set
	 */
	public void setIdChannel(Long idChannel) {
		this.idChannel = idChannel;
	}

	/**
	 * @return the idSender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param idSender the idSender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getHour() {
		return hour;
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}
	
	@Override
    public String toString() {
		StringBuilder builder = new StringBuilder();
        builder.append( "Message:" ).append( this.idMessage )
               .append( " > " ).append( this.message )
               .append("\nin channel: ").append(this.getIdChannel())
               .append("\nsent by:").append(this.getSender());
        return builder.toString();
    }
}