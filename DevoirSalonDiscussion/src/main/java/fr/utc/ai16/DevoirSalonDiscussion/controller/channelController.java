package fr.utc.ai16.DevoirSalonDiscussion.controller;

import java.security.Principal;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.channel;
import fr.utc.ai16.DevoirSalonDiscussion.dao.channelRepository;

@Controller
public class channelController {
	@Autowired
	userRepository userRepository;
	@Autowired
	channelRepository channelRepository;
	
	@RequestMapping("/createchannel")
	public String viewChannelForm(Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
        model.addAttribute("channel", new channel());
        model.addAttribute("user", loggedUser);
		return "channelForm";
	}
	
	@PostMapping("/savenewChannel")
	public String postChannelForm(@ModelAttribute("channel") channel newChannel, Model model, Principal principal,
			@RequestParam(name= "startHourInput") String starHourInput, @RequestParam(name= "endHourInput") String endHourInput) {
		System.out.println("here: " + starHourInput);
		Time startHour = converter(starHourInput);
		System.out.println("after: " + startHour.toString());
		Time endHour = converter(endHourInput);
		user loggedUser = userRepository.getUserByMail(principal.getName());
		System.out.println(newChannel.toString());
		channel channel = channelRepository.findByIdOwnerAndChannelName(loggedUser.getIdUser(), newChannel.getChannelName());
		if( channel != null) {
			model.addAttribute("creationError", "Channel Name Already Exists");
			model.addAttribute("channel", new channel());
			model.addAttribute("user", loggedUser);
			return "channelForm";
		}
		newChannel.setStartHour(startHour);
		newChannel.setEndHour(endHour);
		newChannel.setIdOwner(loggedUser.getIdUser());
		channelRepository.save(newChannel);
		return "redirect:/my_channels";
	}
	
	@PostMapping("/deleteChannel")
	public String postChannelForm(@RequestParam("selected") Long idChannel, Model model, Principal principal) {
		channel channel = channelRepository.getById(idChannel);
		channelRepository.delete(channel);
		return "redirect:/my_channels";
	}
	
	@PostMapping("/adminChannel")
	public String viewChannelAdmin(@RequestParam("selected") Long idChannel, Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
		channel channel = channelRepository.getById(idChannel);
		List<user> users = channel.getUsers();
		model.addAttribute("user", loggedUser);
		model.addAttribute("users", users);
		model.addAttribute("channel", channel);
		return "channelAdmin";
	}
	
	@PostMapping("/inviteUser")
	public String addUserToChannel(@RequestParam("selected") String channelName, @RequestParam("userMail") String userMail, Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
		channel channel = channelRepository.findByIdOwnerAndChannelName(loggedUser.getIdUser(), channelName);
		user user = userRepository.getUserByMail(userMail);
		List<user> users = channel.getUsers();
		model.addAttribute("user", loggedUser);
		model.addAttribute("users", users);
		model.addAttribute("channel", channel);
		if (user == null) {
			model.addAttribute("userError", "User not Found");
			return "channelAdmin";
		}
		if (channel.getUsers().contains(user)) {
			model.addAttribute("userInvError", "User Already Invited");
			return "channelAdmin";
		}
		System.out.println(channel.getIdChannel());
		user.addChannel(channel);
		userRepository.save(user);
		model.addAttribute("invited", user.getFirstName());
		return "channelAdmin";
	}
	
	@PostMapping("/removeUser")
	public String removeUserFromChannel(@RequestParam("selected") String channelName, @RequestParam("removal") Long userId, Model model, Principal principal) {
		user loggedUser = userRepository.getUserByMail(principal.getName());
		channel channel = channelRepository.findByIdOwnerAndChannelName(loggedUser.getIdUser(), channelName);
		user user = userRepository.getById(userId);
		System.out.println("entering removal of:" + user.getFirstName() + "in channel: " + channel.getChannelName());
		user.removeChannel(channel);
		userRepository.save(user);
		List<user> users = channel.getUsers();
		model.addAttribute("user", loggedUser);
		model.addAttribute("users", users);
		model.addAttribute("channel", channel);
		model.addAttribute("removed", user.getFirstName());
		return "channelAdmin";
	}
	public Time converter(String hora) {
		DateFormat formatter = new SimpleDateFormat("HH:mm");
		Time hour = null;
		 try {
		        hour = new java.sql.Time(formatter.parse(hora).getTime());
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }
		return hour;
	}
}
