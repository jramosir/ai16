package fr.utc.ai16.DevoirSalonDiscussion.securingweb;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
 
public class MyUserDetails implements UserDetails {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private user user;
     
    public MyUserDetails(user user) {
        this.user = user;
    }
    
    public user getUser() {
    	return this.user;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
    	return List.of(() -> "read"); 
    }
 
    @Override
    public String getPassword() {
        return user.getPassword();
    }
 
    @Override
    public String getUsername() {
        return user.getMail();
    }
 
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return true;
    }
 
}