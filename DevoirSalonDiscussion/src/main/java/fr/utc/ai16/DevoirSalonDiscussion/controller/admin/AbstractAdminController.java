package fr.utc.ai16.DevoirSalonDiscussion.controller.admin;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;


@RequestMapping("/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")

public class AbstractAdminController {

}
