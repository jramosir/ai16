package fr.utc.ai16.DevoirSalonDiscussion.websocket;

import fr.utc.ai16.DevoirSalonDiscussion.dao.messageRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.channelRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.channel;
import fr.utc.ai16.DevoirSalonDiscussion.models.message;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@Controller
public class ChatController {
	@Autowired private SimpUserRegistry simpUserRegistry;
    @Autowired private SimpMessagingTemplate messagingTemplate;
    @Autowired private messageRepository messageRepository;
    @Autowired private channelRepository channelRepository;
    @Autowired private userRepository userRepository;


    @GetMapping("/chat/{idChannel}")
    public String findMessage (@PathVariable("idChannel") Long idChannel, Model model, Principal principal, HttpSession session) {
    	//get current authentified user
		user loggedUser = userRepository.getUserByMail(principal.getName());
		channel currentChannel = channelRepository.getById(idChannel);
		List<message> channelMessages = messageRepository.findByIdChannel(idChannel);
		//killing a fly with a brick as usual
		session.setAttribute("channel", idChannel);
		model.addAttribute("channel", currentChannel);
        model.addAttribute("user", loggedUser);
		model.addAttribute("users", getWebSocketUsers());
        model.addAttribute("messages", channelMessages);
        return "chat";
    }
    
    
	public Set<SimpUser> getWebSocketUsers() { 
	    return simpUserRegistry.getUsers();
	}
    
}