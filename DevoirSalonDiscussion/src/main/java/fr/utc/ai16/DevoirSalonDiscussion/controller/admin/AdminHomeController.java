package fr.utc.ai16.DevoirSalonDiscussion.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import fr.utc.ai16.DevoirSalonDiscussion.dao.channelRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;

@Controller
public class AdminHomeController extends AbstractAdminController {
	@Autowired
	private userRepository userRepository;

	@Autowired
	private channelRepository channelRepository;

	@GetMapping({ "/", "" })
	public String getAdminHome() {
		return "admin/home";

	}

//	@GetMapping("/admin-channels")
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	public String getChannels(Model model) {
//		System.out.println("enter channels map");
//		List<channel> channels = channelRepository.findAll();
//		model.addAttribute("channels", channels);
//		return "channel_list";
//	}
}