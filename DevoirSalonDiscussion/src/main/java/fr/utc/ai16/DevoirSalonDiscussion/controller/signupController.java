package fr.utc.ai16.DevoirSalonDiscussion.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.securingweb.UserDetailsServiceImpl;
import fr.utc.ai16.DevoirSalonDiscussion.securingweb.MyUserDetails;
@Controller
class signupController {
	@Autowired
	private userRepository userRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping("/signup")
	public String showRegister(Model model ){
        model.addAttribute( "user", new user());
        return "signup"; 
    }

	
	@PostMapping("/save")
	public String register( @ModelAttribute("user") user user, Model model) {
		System.out.println("registering newuser:" + user.toString() + user.getPassword());
		if (userExists(user.getMail()))
        {
            model.addAttribute("user", new user());
            model.addAttribute("registrationError", "User name already exists.");
            System.out.println("User already exists.");
            return "signup";
        }
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		//somehow sql default true value not applied when creating new user... add manually
		user.setActive(true);
		userRepo.save(user);
		System.out.println("registered:" + user.toString());
		model.addAttribute("user", user);
		return "welcome";
	}
	
    private boolean userExists( String mail )
    {
        
    	System.out.println( "Checking if user exists: " + mail );
        
        // check the database if the user already exists
    	boolean exists;
        if (userRepo.getUserByMail(mail) == null) {
        	exists = false;
        }
        else {
        	exists = true;
        }
        System.out.println( "User: " + mail + ", exists: " + exists );
        return exists;
    }
}