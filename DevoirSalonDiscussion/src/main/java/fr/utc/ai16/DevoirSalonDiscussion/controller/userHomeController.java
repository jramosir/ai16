package fr.utc.ai16.DevoirSalonDiscussion.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.channelRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.channel;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.securingweb.UserDetailsServiceImpl;
import fr.utc.ai16.DevoirSalonDiscussion.securingweb.MyUserDetails;

@Controller
public class userHomeController {
    @Autowired
    private userRepository userRepository;
    @Autowired
    private channelRepository channelRepository;
    
	@GetMapping("/home")
	public String showUserHome(Model model, Principal principal){
		//get current authentified user
		user loggedUser = userRepository.getUserByMail(principal.getName());
        model.addAttribute("loggedUser", loggedUser);
        System.out.println("logged as:");
        System.out.println(loggedUser.toString());
        List<channel> userChannels = channelRepository.findByIdOwner(loggedUser.getIdUser());
        List<channel> invitedChannels = loggedUser.getChannels();
        model.addAttribute("userChannels", userChannels);
        model.addAttribute("invitedChannels", invitedChannels);
        return "home";
    }
	
	@GetMapping("/my_channels")
	public String showUserChannels(Model model, Principal principal){
		//get current authentified user
		user loggedUser = userRepository.getUserByMail(principal.getName());
        model.addAttribute("user", loggedUser);
        List<channel> userChannels = null;
        List<channel> invitedChannels = loggedUser.getChannels();
        if (loggedUser.isAdmin()){
        	userChannels = channelRepository.findAll();
        } else {
        	userChannels = channelRepository.findByIdOwner(loggedUser.getIdUser());
        }
        model.addAttribute("userChannels", userChannels);
        model.addAttribute("invitedChannels", invitedChannels);
        return "channel_list";
    }
}
