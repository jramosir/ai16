package fr.utc.ai16.DevoirSalonDiscussion.websocket;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.messageRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.message;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

@Controller
public class WebSocketController {
    @Autowired
    private userRepository userRepository;
    @Autowired
    private messageRepository messageRepository;


    @MessageMapping("chat.sendMessage")
    @SendTo("/topic/publicChatRoom")
    public message sendMessage(@Payload message message) {
    	//get current authentified user
    	if (!message.getSender().equals("SpringServer")) {
    		messageRepository.save(message);
    	}
    	System.out.println("send message");
        return message;
    }

    @MessageMapping("chat.addUser")
    @SendTo("/topic/publicChatRoom")
    public message addUser(@Payload message message, SimpMessageHeaderAccessor headerAccessor) {
        // Add user in web socket session
        headerAccessor.getSessionAttributes().put("username", message.getSender());
        if (!message.getSender().equals("SpringClient")) {
    		messageRepository.save(message);
    	}
        System.out.println("send message adduser");
        return message;
    }

}