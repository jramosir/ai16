package fr.utc.ai16.DevoirSalonDiscussion.securingweb;

import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
 
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    private userRepository userRepository;
     
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
    	user user = userRepository.getUserByMail(username);
    	if(user == null) {
			throw new UsernameNotFoundException("User name "+username+" not found");
		}
    	else if(user.isActive() == false) {
			throw new DisabledException("User name "+username+" is inactive");
    	}
		return new org.springframework.security.core.userdetails.User(user.getMail(), user.getPassword(), getGrantedAuthorities(user));
	}

	private Collection<GrantedAuthority> getGrantedAuthorities(user user) {
		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		if(user.isAdmin()) {
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		}
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		return grantedAuthorities;
	}
    
    public void createUser(user user) { 
        userRepository.save(user); 
     } 
 
}