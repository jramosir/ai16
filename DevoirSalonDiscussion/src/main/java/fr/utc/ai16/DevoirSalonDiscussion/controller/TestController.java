package fr.utc.ai16.DevoirSalonDiscussion.controller;
import java.security.Principal;
import java.util.List;
import java.sql.Date;
import java.sql.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  
import fr.utc.ai16.DevoirSalonDiscussion.dao.channelRepository;
import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;
import fr.utc.ai16.DevoirSalonDiscussion.models.channel;

/**
 * @author Santiago RAMOS
 * Testing for DAO models and interface utilities. CRUD
 */
@Controller
public class TestController {
	
	@Autowired
	private userRepository userRepo;
	
	@Autowired
	private channelRepository channelRepo;
	
	@RequestMapping("/test")
	@ResponseBody
	public String test() {
		System.out.println("enter test map users");
		List<user> users = userRepo.findAll();

		users.forEach(user -> {
			System.out.println(user.getFirstName());
			//System.out.println(user.getChannels());
			//System.out.println(user.getSentMessages());
		});
		
		System.out.println("enter test insert");
		user newUser = new user();
		newUser.setFirstName("Jean");
		newUser.setLastName("Pierre");
		newUser.setMail("pierre@test.com");
		newUser.setPassword("123");
		newUser.setAdmin(false);
		newUser.setGender("Male");
		newUser.setActive(true);
		userRepo.save(newUser);
		
		users = userRepo.findAll();
		users.forEach(user -> {
			System.out.println(user.getFirstName());
			//System.out.println(user.getChannels());
			//System.out.println(user.getSentMessages());
		});
		
		System.out.println("enter test delete");
		userRepo.delete(userRepo.getUserByMail("pierre@test.com"));
		users = userRepo.findAll();
		users.forEach(user -> {
			System.out.println(user.getFirstName());
			//System.out.println(user.getChannels());
			//System.out.println(user.getSentMessages());
		});

		System.out.println("enter test findmail");
		user testing = userRepo.getUserByMail("jose1.ramos@utc.fr");
		System.out.println(testing.toString());


		return "ok";
	}
	@RequestMapping("/test/channel")
	@ResponseBody
	public String addChannel(Principal principal) {
		user loggedUser = userRepo.getUserByMail(principal.getName());
		channel testChannel = new channel();
		long millis=System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Time time = new java.sql.Time(millis);
		testChannel.setChannelName("TestingChannel");
		testChannel.setDescription("TestingChannel");
		testChannel.setIdOwner(loggedUser.getIdUser());
		testChannel.setStartDate(date);
		testChannel.setEndDate(sqlDatePlusDays(date, 20));
		testChannel.setStartHour(time);
		testChannel.setEndHour(sqlTimePlusDays(time, 1));
		loggedUser.addChannel(testChannel);
		channelRepo.save(testChannel);
		return "ok";
	}
	private Date sqlDatePlusDays(Date date, int days) {
	    return Date.valueOf(date.toLocalDate().plusDays(days));
	}
	private Time sqlTimePlusDays(Time date, int hours) {
	    return Time.valueOf(date.toLocalTime().plusHours(hours));
	}
}
