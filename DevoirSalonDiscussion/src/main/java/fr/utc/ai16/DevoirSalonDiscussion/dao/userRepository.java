package fr.utc.ai16.DevoirSalonDiscussion.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.utc.ai16.DevoirSalonDiscussion.models.user;

public interface userRepository extends JpaRepository<user, Long> {
	List<user> findByFirstName(String firstName);

	List<user> findByActive(boolean active);

	user findOneByIdUser(Long idUser);

	@Query("SELECT u FROM user u WHERE u.mail = :mail")
	Optional<user> findUserByMail(@Param("mail") String mail);

	@Query("SELECT u FROM user u WHERE u.mail = :mail")
	public user getUserByMail(@Param("mail") String mail);

	@Query("SELECT u FROM user u WHERE u.mail = :mail AND u.password = :password")
	public user findByMailAndPassword(@Param("mail") String mail, @Param("password") String password);

}
