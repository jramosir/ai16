package fr.utc.ai16.DevoirSalonDiscussion.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

@Controller
public class AdminUsersListController extends AbstractAdminController {

	@Autowired
	private userRepository userRepository;

	@GetMapping("/users-list")
	public String getUsersList(Model model) {
		List<user> users = userRepository.findAll();
		model.addAttribute("users", users);
		return "admin/usersList";
	}

	@GetMapping("/users-list-disabled")
	public String getUsersListDisabled(Model model) {
		List<user> users = userRepository.findByActive(false);
		model.addAttribute("users", users);
		return "admin/usersListDisabled";
	}

	@PostMapping("/edit-user")
	@ResponseBody
	public ResponseEntity<?> postEditUser(user editedUser) {

		Long userID = editedUser.getIdUser();

		// récupération de l'utilisateur existant en bd
		user existingUser = userRepository.findOneByIdUser(userID);

		// modification des champs
		existingUser.setFirstName(editedUser.getFirstName());
		existingUser.setLastName(editedUser.getLastName());
		existingUser.setMail(editedUser.getMail());
		existingUser.setGender(editedUser.getGender());
		existingUser.setAdmin(editedUser.isAdmin());
		existingUser.setActive(editedUser.isActive());

		// sauvegarde des modifications
		userRepository.save(existingUser);

		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/delete-user/{userID}")
	public ResponseEntity<?> deleteUser(@PathVariable("userID") Long userID) {
		// récupération de l'utilisateur existant en bd
		user existingUser = userRepository.findOneByIdUser(userID);

		// suppression en bdd de l'utilisateur existant
		userRepository.delete(existingUser);

		return ResponseEntity.ok().build();
	}

}