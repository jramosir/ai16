package fr.utc.ai16.DevoirSalonDiscussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevoirSalonDiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevoirSalonDiscussionApplication.class, args);
	}

}
