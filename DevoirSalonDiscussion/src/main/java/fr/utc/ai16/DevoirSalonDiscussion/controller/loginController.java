package fr.utc.ai16.DevoirSalonDiscussion.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

@Controller
@RequestMapping("/login")
public class loginController {
    
    @GetMapping
    public String login(HttpServletRequest request, HttpSession session) {
        session.setAttribute(
           "error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION")
        );
        return "login";
    }
    
    private String getErrorMessage(HttpServletRequest request, String key) {
        Exception exception = (Exception) request.getSession().getAttribute(key); 
        String error = ""; 
        if (exception instanceof BadCredentialsException) { 
           error = "Invalid username and password!"; 
        } else if (exception instanceof LockedException) { 
           error = exception.getMessage();
        } else if (exception instanceof DisabledException) { 
            error = exception.getMessage();
        } else { 
           error = "Invalid username and password!";
        } 
        return error;
    }
}
