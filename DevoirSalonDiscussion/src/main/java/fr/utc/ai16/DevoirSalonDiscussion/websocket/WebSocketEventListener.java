package fr.utc.ai16.DevoirSalonDiscussion.websocket;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.message;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

@Component
public class WebSocketEventListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;
    @Autowired private userRepository userRepository;

    
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        
        String username = headerAccessor.getUser().getName();
        Long channelId = (Long) headerAccessor.getSessionAttributes().get("channel");
		long millis=System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Time time = new java.sql.Time(millis);
        if(username != null) {
            logger.info("User Disconnected : " + username);

            message chatMessage = new message();
            chatMessage.setMessage(username + "- has disconnected");
            chatMessage.setSender("SpringServer");
            chatMessage.setIdChannel(channelId);
            chatMessage.setDate(date);
            chatMessage.setHour(time);
            messagingTemplate.convertAndSend("/topic/publicChatRoom", chatMessage);
        }
    }
    
}