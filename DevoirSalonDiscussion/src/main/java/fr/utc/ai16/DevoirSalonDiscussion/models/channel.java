package fr.utc.ai16.DevoirSalonDiscussion.models;
import java.sql.Time;
import java.sql.Date;
import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Santiago RAMOS
 * This class is a spring entity used to map a channel class object with a channel database table in order to implement CRUD methods efficiently
 */
@Entity
@Table(name = "ai16_channels")
public class channel {
	/*to generate increasing values for new users id
	 *link to database column is automatically resolved if the attribute name is the same
	 *otherwise it must be declared
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_channel")
	private Long idChannel;
	@Column(name="id_Owner")
	private Long idOwner;
	@Column(name="channel_name")
	private String channelName;
	private String description;
	//sql Date type
	@Column(name="start_date")
	private Date startDate;
	@Column(name="end_date")
	private Date endDate;
	//sql Time type
	@Column(name="start_hour")
	private Time startHour;
	@Column(name="end_hour")
	private Time endHour;
    
    @ManyToMany(mappedBy = "channels", fetch = FetchType.LAZY)
    private List<user> users = new ArrayList<>();

 
	public channel(){
		//Default constructor with no argument	
	}
	public Long getIdChannel() {
		return idChannel;
	}
	public void setIdChannel(Long idChannel) {
		this.idChannel = idChannel;
	}
	public Long getIdOwner() {
		return idOwner;
	}
	public void setIdOwner(Long long1) {
		this.idOwner = long1;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Time getStartHour() {
		return startHour;
	}
	public void setStartHour(Time startHour) {
		this.startHour = startHour;
	}
	public Time getEndHour() {
		return endHour;
	}
	public void setEndHour(Time endHour) {
		this.endHour = endHour;
	}
	public List<user> getUsers() {
		//returns list of users with access to the channel
        return users;
    }
	
	public boolean isValid() {
		long millis=System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(millis);
		java.sql.Time time = new java.sql.Time(millis);
		if (date.compareTo(this.getEndDate()) >= 0 && time.compareTo(this.getEndHour()) >= 0) {
			return false;
		}
		else {
			return true;
		}
	}
	@Override
    public String toString() {
		StringBuilder builder = new StringBuilder();
        builder.append( "\nChannel:" ).append( this.idChannel )
               .append( " > Owner:" ).append( this.idOwner ).append( "\n" )
               .append(this.channelName).append(":").append(this.description)
               .append(":").append(this.startDate).append(":").append(this.startHour);
        return builder.toString();
    }
}
