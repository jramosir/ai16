package fr.utc.ai16.DevoirSalonDiscussion.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.utc.ai16.DevoirSalonDiscussion.dao.userRepository;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

@Controller
public class AdminCreateUserController extends AbstractAdminController {

	@Autowired
	private userRepository userRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@GetMapping("/create-user")
	public String getCreateUser() {
		return "admin/createUser";
	}

	@PostMapping("/create-user")
	@ResponseBody
	public ResponseEntity<?> register(user createdUser) {
		String encodedPassword = passwordEncoder.encode("motdepasse");
		createdUser.setPassword(encodedPassword);
		userRepo.save(createdUser);
		return ResponseEntity.ok().build();
	}

}