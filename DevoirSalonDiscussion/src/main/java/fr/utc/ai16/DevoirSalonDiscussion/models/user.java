package fr.utc.ai16.DevoirSalonDiscussion.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Santiago RAMOS
 * This class is a spring entity used to map a user class object with a user database table in order to implement CRUD methods efficiently
 * ATTENTION: Naming convention in important while using this god forsaken framework otherwise repository methods don't work........
 */
@Entity
@Table(name = "ai16_users")
public class user {
	
	/*to generate increasing values for new users id
	 *link to database column is automatically resolved if the attribute name is the same
	 *otherwise it must be declared
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_user")
	private Long idUser;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	private String mail;
	private String password;
	private String gender;
	private boolean admin;
	private boolean active;
	
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable( name = "ai16_channel_user_associations",
                joinColumns = @JoinColumn( name = "id_user" ),
                inverseJoinColumns = @JoinColumn( name = "id_channel" ) )
    private List<channel> channels = new ArrayList<>();

    
	public user(){
		//Default constructor with no argument.
		//no overloaded constructor because all values are not null in the table, therefore all values must be filled anyways.
	}
	/*
	 * Setter and Getter for each class attribute
	 */
	public Long getIdUser() {
		return idUser;
	}
	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public List<channel> getChannels() {
        return channels;
    }

    public void addChannel(channel channel) {
		channels.add( channel );
		channel.getUsers().add( this );
	}

	public void removeChannel(channel channel) {
		channels.remove( channel );
		channel.getUsers().remove( this );
	}
    
	@Override
    public String toString() {
		StringBuilder builder = new StringBuilder();
        builder.append( "User:" ).append( this.idUser )
               .append( " > " ).append( this.firstName ).append( "." )
               .append(this.lastName);
        return builder.toString();
    }

}
