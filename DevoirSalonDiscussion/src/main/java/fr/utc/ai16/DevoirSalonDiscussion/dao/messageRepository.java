package fr.utc.ai16.DevoirSalonDiscussion.dao;

/**
 * @author Santiago RAMOS
 *
 */
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.utc.ai16.DevoirSalonDiscussion.models.channel;
import fr.utc.ai16.DevoirSalonDiscussion.models.message;
import fr.utc.ai16.DevoirSalonDiscussion.models.user;

public interface messageRepository extends JpaRepository<message, Long> {
	List <message> findByIdChannel(Long idChannel);
	//@Query("SELECT m FROM message m WHERE m.sender = :sender AND m.channel = :channel\"")
	//message findSenderChannelMessages(@Param("sender") user user, @Param("channel") channel channel);
	//user findByMailAndPassword(@Param("mail") String mail, @Param("password") String password);
}

