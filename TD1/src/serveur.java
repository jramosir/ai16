import java.net.*;
import static java.lang.Math.floor;
import java.util.logging.*;
import java.io.*;

public class serveur {
	public static int jeu_ordi (int nb_allum, int prise_max){
		//Fonction de prise d'allumettes par l'ordinateur
		//Returns int nombre d'allumettes � prendre
		int prise = 0;
		int s = 0;
		float t = 0;
		s = prise_max + 1;
		t = ((float) (nb_allum - s)) / (prise_max + 1);
		while (t != floor(t)){
			s--;
			t = ((float) (nb_allum-s)) / (prise_max + 1);
		}
		prise = s - 1;
		if (prise == 0)
		prise = 1;
		return (prise);
	}
	public static void afficher_allu(int n){
		int i;
		System.out.print("\n");
		for (i=0; i<n ;i++)
			System.out.print("  o");
		System.out.print("\n");
		for (i=0; i<n; i++)
			System.out.print("  |");
		System.out.print("\n"); 
		for (i=0; i<n; i++)
			System.out.print("  |");
		System.out.print("\n"); 
	}
	public static void main(String[] args) {
		//initialization de variables pour le jeu
		int nb_max_d=0; /*nbre d'allumettes maxi au d�part*/
		int nb_allu_max=0; /*nbre d'allumettes maxi que l'on peut tirer au maxi*/
		int qui=0; /*qui joue? 0=Nous --- 1=PC*/
		int prise=0; /*nbre d'allumettes prises par le joueur*/
		int nb_allu_rest=0; /*nbre d'allumettes restantes*/
		try {
			ServerSocket conn = new ServerSocket(10080);
			System.out.println("Serveur Demarr�.............");
			//To close socket if loop continues undesired: *in console*
			//netstat -ano | findstr :10080
			//taskkill /pid XXXX /F
			Socket client = conn.accept();
			System.out.println("Nouvelle connexion accept�......");
			//class DataInputStream permet des echanges plus pratiques
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			DataInputStream in = new DataInputStream(client.getInputStream());
			//on a pass� les variables d'initialization sous format specifique
			String []info_client=in.readUTF().split("/");
            nb_allu_max=Integer.parseInt(info_client[0]);
            nb_max_d=Integer.parseInt(info_client[1]);
            qui=Integer.parseInt(info_client[2]);
            System.out.println("info client:"+nb_allu_max+"/"+nb_max_d+"/"+ qui);
            nb_allu_rest=nb_max_d;
			do{
				System.out.println("\nNombre d'allumettes restantes :"+nb_allu_rest);
	            afficher_allu(nb_allu_rest);
	            if (qui==0){
	            	//l'utilisateur saisie le nombre � piocher et le serveur ecoute
	            	System.out.println("\nserveur en attente de utilisateur");
	                prise=in.readInt();
	                System.out.println("\nprise de l'utilisateur: "+prise);
	            }
	            else{
	                prise = jeu_ordi(nb_allu_rest , nb_allu_max);
	                out.writeInt(prise);
	                out.flush();
	                System.out.println ("\nPrise de l'ordi :"+prise);  
	            }
	            qui=(qui+1)%2;
	            nb_allu_rest= nb_allu_rest - prise;
	        }
	        while (nb_allu_rest >0);
			System.out.println("Fin de jeu");
			/*in.close();
			out.close();
			conn.close();*/
		} catch (IOException ex) {
			Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
		}
	} 
}