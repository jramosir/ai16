import java.net.*;
import java.util.Scanner;
import java.util.logging.*;
import java.io.*;

public class client {
	/*
	 * R�aliser un envoi d�un message � partir du programme. en utilisant les
	 * m�thodes fournies par la class OutputStream. ce programme execute le
	 * client(utilisateur) de jeu d'allumettes
	 */
	public static void saisirInfo() {
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("Nombre d'allumettes disposées entre les deux joueurs (entre 10 et 60) :");
			nb_max_d = sc.nextInt();
		}

		while ((nb_max_d < 10) || (nb_max_d > 60));
		do {
			System.out.println("\nNombre maximal d'allumettes que l'on peut retirer : ");
			nb_allu_max = sc.nextInt();
			if (nb_allu_max >= nb_max_d)
				System.out.println("Erreur !");
		} while ((nb_allu_max >= nb_max_d) || (nb_allu_max == 0));
		/*
		 * On répète la demande de prise tant que le nombre donné n'est pas correct
		 */
		do {
			System.out.println("\nQuel joueur commence? 0--> Joueur ; 1--> Ordinateur : ");
			qui = sc.nextInt();

			if ((qui != 0) && (qui != 1))
				System.out.println("\nErreur");
		} while ((qui != 0) && (qui != 1));
		//sc.close();
	}

	public static void afficher_allu(int n) {
		int i;
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  o");
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  |");
		System.out.print("\n");
		for (i = 0; i < n; i++)
			System.out.print("  |");
		System.out.print("\n");
	}

	// global variable car plus simple a gerer juste pour illustrer l'exercice
	static int nb_max_d = 0; /* nbre d'allumettes maxi au d�part */
	static int nb_allu_max = 0; /* nbre d'allumettes maxi que l'on peut tirer au maxi */
	static int qui = 0; /* qui joue? 0=Nous --- 1=PC */

	public static void main(String[] args) {
		// init des variables pour le jeu
		int prise = 0; /* nbre d'allumettes prises par le joueur */
		int nb_allu_rest = 0; /* nbre d'allumettes restantes */
		saisirInfo();
		nb_allu_rest = nb_max_d;
		try {
			Scanner sc = new Scanner(System.in);
			// Creation de socket client-serveur
			Socket client = new Socket("localhost", 10080);
			// Ouverture de iostream
			System.out.println("\nClient Connect�........");
			DataInputStream in = new DataInputStream(client.getInputStream());
			DataOutputStream out = new DataOutputStream(client.getOutputStream());
			// passer parametres de jeu au serveur sous forme de string
			out.writeUTF("" + nb_allu_max + "/" + nb_max_d + "/" + qui);
			// debut du jeu
			do {
				System.out.println("\nNombre d'allumettes restantes: " + nb_allu_rest);
				afficher_allu(nb_allu_rest);
				if (qui == 0) {
					// L'utilisatuer Pioche des allumettes
					do {
						System.out.println("\nCombien d'allumettes souhaitez-vous piocher ? ");
						prise = sc.nextInt();
						if ((prise > nb_allu_rest) || (prise > nb_allu_max)) {
							System.out.println("Erreur !\n");
						}
					} while ((prise > nb_allu_rest) || (prise > nb_allu_max));
					/*
					 * On répète la demande de prise tant que le nombre donné n'est pas correct
					 */
					// envoyer au serveur le nombre d'allumettes que l'utilisateur a pioch�
					out.writeInt(prise);
					out.flush();
				} else {
					System.out.println("\nClient en attente de serveur");
					prise = in.readInt();
					System.out.println("\nPrise de l'ordi :" + prise);
				}
				qui = (qui + 1) % 2;
				nb_allu_rest = nb_allu_rest - prise;
			} while (nb_allu_rest > 0);
			if (qui == 0) /* Cest à nous de jouer */
				System.out.println("\nVous avez gagné!\n");
			else
				System.out.println("\nVous avez perdu!\n");
			/*
			 * in.close(); out.close(); client.close(); sc.close();
			 */
		} catch (IOException ex) {
			Logger.getLogger(Socket.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
