# AI16 - P22
## Membres
LUCAS Clément
RAMOS IRETA José Santiago
YOUSSEF Laëtitia

## Description
TD/TP du cours AI16
chaque dossier est associé à un projet eclipse different avec ses propres fichiers README

## Installation
##### Pour télécharger:
Tapez les commandes successives dans le repertoire de votre choix:
1. SSH
```bash
git clone git@gitlab.utc.fr:jramosir/ai16.git
```
2. HTTP
```bash
git clone https://gitlab.utc.fr/jramosir/ai16.git
```
```bash
cd ai16											#on se place dans le répertoire git
```
##### Pour télécharger les mises à jour (il faut le faire avant toute modification)
```bash 
git pull
```
##### Pour mettre à jour ses modifications
```bash
git commit -a -m "message associé au commit"        # commit = sauvegarder en local (l'option -a veut dire all)
git push 											# pour uploader tous le code modifié (commited)
```

##### Pour ajouter tous les nouveaux fichiers créés localement au projet
```bash
git add * 										    # à faire avant le nouveau commit
```
