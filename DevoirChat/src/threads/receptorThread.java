package threads;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Vector;
import java.util.regex.Pattern;

import serveur.serveur;
/*
 * receptorThread lance le processus en parallele pour un client qui en charge
 * de recevoir les messages recus par le serveur des differents clients
 */
public class receptorThread extends Thread{
	Socket client = null;
	String nom = "";
	DataInputStream in = null;
	DataOutputStream output = null;
	boolean messageFlag = true; //cette variable indique si on a des nouveaux messages

	public receptorThread(Socket client, String clientNickName) throws IOException {
		/**
		 * 
		 * @param client: socket de communication du client
		 * @param nom: nom du client
		 * @throws IOException
		 */
		//initialisation d'attributs d'un client dans le constructeur
		this.client = client; //socket
		this.in = new DataInputStream(client.getInputStream());
		this.output = new DataOutputStream(client.getOutputStream());
		this.nom = clientNickName;
	}
	//accesseurs de socket et nom
	public Socket getClientSocket() {
		return this.client;
	}

	public String getNom() {
		return nom;
	}

	public boolean getMessageFlag() {
		return this.messageFlag;
	}

	public void setMessageFlag(boolean res) {
		this.messageFlag = res;
	}

	public void closeConnections() throws IOException {
		this.in.close();
		this.output.close();
		this.client.close();
	}

	@Override

	public void run() {
		String[] messageReceived = null;
		String delimiter = Pattern.quote("|");
		try {
			while (getMessageFlag()) {
				//table de messages recus
				messageReceived = in.readUTF().split(delimiter);
				//si on se deconnecte en ecrivant exit, on notifie la console
				if (messageReceived[2].equals("ok") && messageReceived[0].equals(getNom())) {
					System.out.println("Tu n'est plus dans le chat");
					setMessageFlag(false);
				}
				//affichage des messages
				System.out.println(messageReceived[0] + " a dit: " + messageReceived[1]);

			}
			closeConnections();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
