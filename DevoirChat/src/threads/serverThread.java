package threads;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Pattern;
import serveur.serveur;



public class serverThread extends Thread{

	private Map<String, Socket> connectedClients = null;
	private Socket client = null;
	private String clientName = null;
	private volatile boolean finishListen = false;
	private DataInputStream in = null;
	private DataOutputStream out = null;

	public serverThread(Socket currentClient, String clientName) throws IOException {
		this.clientName = clientName;
		this.client = currentClient;
		this.in = new DataInputStream(currentClient.getInputStream());
		this.out = new DataOutputStream(currentClient.getOutputStream());
	}

	public Socket getCommunicationSocket() {
		return client;
	}

	public String getUserName() {
		return clientName;
	}

	public boolean getFinishListen() {
		return this.finishListen;
	}

	public void setFinishListen(boolean result) {
		this.finishListen = result;
	}

	public void spreadMessageClient(String message, String sender) throws IOException {
		/**
		 * L'objectif de la methode c'est d'envoyer le message recu par le serveur a
		 * tous les clients connect�s.
		 * 
		 * @param message contenu du message qui doit etre envoye
		 * @param sender  surnom du client qui envoie le message
		 * @throws IOException
		 */
		DataOutputStream outFunction = null;
		String delimiteur = Pattern.quote("|");
		String[] messageReceived = message.split(delimiteur);
		connectedClients = serveur.getMapClients();
		if (messageReceived[1].equals("ok")) {
			connectedClients.remove(getUserName());
			serveur.setMapClients(connectedClients);
		}
		for (Map.Entry<String, Socket> client : connectedClients.entrySet()) {
			if (!client.getKey().equals(sender)) {
				outFunction = new DataOutputStream(((Socket) client.getValue()).getOutputStream());
				if (message.equals("exit")) {
					outFunction.writeUTF(sender + "|" + message);
				} else {
					outFunction.writeUTF(sender + "|" + message);
				}

			}

		}

	}

	public void closeConnections() throws IOException {
		this.in.close();
		this.out.close();
		this.client.close();
	}

	@Override
	public void run() {
		String receivedMessage = "";

		try {

			while (!getFinishListen()) {
				receivedMessage = in.readUTF();
				if (receivedMessage.equals("exit")) {
					setFinishListen(true);
					spreadMessageClient(getUserName() + " s'est deconnecte|ok", "Server");

				} else {
					spreadMessageClient(receivedMessage + "|no", getUserName());
				}

			}

			closeConnections();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
