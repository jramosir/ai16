package threads;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;


public class writeThread extends Thread {
/*
 * lancer un processus en parallele pour ecrire de messages et les envoyer aux clients
 * pendant qu'on est aussi en attente des messages d'autres clients
 */
	Socket client = null;
	DataInputStream in = null;
	DataOutputStream output = null;
	volatile boolean finishThread = false; //flag pour deconnection et fermeture de thread

	public writeThread(Socket client) throws IOException {
		//constructeur, initaliser les variables
		this.client = client; //socket
		this.in = new DataInputStream(client.getInputStream());
		this.output = new DataOutputStream(client.getOutputStream());
	}
	//Accesseurs
	public Socket getClientSocket() {
		return this.client;
	}
	public boolean getFinishThread() {
		return this.finishThread;
	}

	public void setFinishThread(boolean result) {
		this.finishThread = result;
	}

	public void closeConnections() throws IOException {
		this.in.close();
		this.output.close();
		this.client.close();
	}
	
	@Override
	
	public void run() {
		Scanner scanner = new Scanner(System.in);
		String newMessage = "";
		try {
			while (!getFinishThread()) {

				newMessage = scanner.nextLine();
				output.writeUTF(newMessage);
				//pour sortir de chat on ecris exit
				if (newMessage.equals("exit")) {
					setFinishThread(true);

				}

			}

			closeConnections();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
