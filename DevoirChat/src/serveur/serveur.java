package serveur;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;

import threads.*;
/*
 * La classe serveur sert a gerer l'etablissement de connexions des clients
 * 
*/
public class serveur {
	//structure dynamique hashmap pour stocker les sockets(outputsream) des clients
	public static Map<String, Socket> clientsSockets = new HashMap<String, Socket>();
	//Accesseurs de Map: get pour recuperer les clients connectes
	public static Map<String, Socket> getMapClients() {
		return clientsSockets;
	}
	//set pour modifier la liste courant de clients
	public static void setMapClients(Map<String, Socket> newMappingClients) {
		serveur.clientsSockets = newMappingClients;
	}


	public static void main(String[] args) throws IOException {

		ServerSocket connectionSocket = new ServerSocket(10080);
		DataInputStream in = null;
		DataOutputStream out = null;
		String nameClient = "";
		serverThread clientThread = null;
		
		while (true) {
			//attente de nouvelles connexions
			Socket communicationSocket = connectionSocket.accept();
			out = new DataOutputStream(communicationSocket.getOutputStream());
			in = new DataInputStream(communicationSocket.getInputStream());
			nameClient = in.readUTF();
			//verification de disponibilit� du nom dans la table de clients
			while (!serveur.isNameAvailable(nameClient)) {
				out.writeUTF("no");
				nameClient = in.readUTF();
			}
			out.writeUTF("ok");
			//si le nom est disponible, on l'ajoute � la table
			clientsSockets.put(nameClient, communicationSocket);
			//on notifie les clients connect�es qu'une nouvelle client � rejoint
			serveur.sendClientMessage("", nameClient, communicationSocket);
			//on passe le socket au nouveau thread client qui va le gerer
			clientThread = new serverThread(communicationSocket, nameClient);
			clientThread.start();
		}
	}

	public static boolean isNameAvailable(String name) {
		boolean availability = true;
		//socket table is empty
		if (clientsSockets.size() == 0)
			return true;
		//iterate on table and check each name
		for (Map.Entry<String, Socket> client : clientsSockets.entrySet()) {
			if (name.equals(client.getKey())) {
				availability = false;
			}
		}
		return availability;
	}
	
	public static void sendClientMessage(String message, String nameSender, Socket clientSocket) throws IOException{
		DataOutputStream out = null;
		//si le socket n'est pas instanci� on le cr�e � nouveau
		if (clientSocket != null) {
			out = new DataOutputStream(clientSocket.getOutputStream());
		}
		//on cr�e un nouveau Map des clients actuellement connect�s
		Map<String, Socket> connectedClients = null;
		connectedClients = serveur.getMapClients();
		for (Map.Entry<String, Socket> client : connectedClients.entrySet()) {
			//renvoyer le message aux autres clients
			if (!client.getKey().equals(nameSender)) {
				out = new DataOutputStream(((Socket) client.getValue()).getOutputStream());
				out.writeUTF("Server|" + nameSender + " a  rejoint la conversation|no");

			}

		}

	}

}
