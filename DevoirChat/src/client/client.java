package client;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import threads.*;

/*
 * client est une classe destin� � simuler le comportement d'un client.
 * La premiere etape c'est d'etablir la connexion au serveur en lui donnant un identifiant
 * Ensuite la classe va lancer 2 threads, un pour gerer l'envoi de messages
 * l'autre pour ecouter les messages provenant des autres clients
 */
public class client {
	//throws exception plus facile � gerer que try catch
	public static void main(String[] args) throws UnknownHostException, IOException {
		//Demande de connexion au serveur
		Socket communicationSocket = new Socket("localhost", 10080);
		//initialisation de variables et iostream
		DataInputStream in = new DataInputStream(communicationSocket.getInputStream());
		DataOutputStream out = new DataOutputStream(communicationSocket.getOutputStream());
		writeThread writeThread = null;
		receptorThread receptorThread = null;
		//saisie d'un nouveau client avec un nom
		System.out.println("Bienvenue! Entrez votre nom: ");
		Scanner scanner = new Scanner(System.in);
		String clientName = scanner.nextLine();
		//verification de disponibilite du nom
		String availableName = "";
		//envoyer le nom du client au serveur
		out.writeUTF(clientName);

		do {
			//verification de disponibilite du nom
			availableName = in.readUTF();
			if (availableName.equals("no")) {
				System.out.println("Le nom est deja utilis�");
				System.out.println("Entrez un autre nom:");
				clientName = scanner.nextLine();
				out.writeUTF(clientName);
			}
		} while (!availableName.equals("ok"));
		System.out.println("Votre nom est: " + clientName);
		//connexion etablie, on lance les threads de gestion de messages
		writeThread = new writeThread(communicationSocket);
		receptorThread = new receptorThread(communicationSocket, clientName);
		writeThread.start();
		receptorThread.start();

	}

}
